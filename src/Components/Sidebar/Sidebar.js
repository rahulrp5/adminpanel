import React from "react";
import {Link} from 'react-router-dom';
import "./Sidebar.css";
import {Home,Timeline,TrendingDown, Person, Receipt,Assessment, Mail,Feedback, Message} from '@material-ui/icons';
export default function Sidebar() {
  return (
    <div className="sidebar">
      <div className="sidebarWrapper">
        <div className="sidebarMenu">
         <h3 className="sidebarTitle">Dashboard</h3> 
         <ul className="sidebarList">
             <Link to="/" className="Link">
             <li className="sidebarListItem active">
             <Home className="sidebarIcon"/> Home
             </li>
             </Link>
             <li className="sidebarListItem">
                <Timeline className="sidebarIcon"/> Analytics
             </li>
             <li className="sidebarListItem">
                 <TrendingDown className="sidebarIcon"/>Sales
             </li>
        </ul> 
        </div>
        <div className="sidebarMenu">
         <h3 className="sidebarTitle">Quick Menu</h3> 
         <ul className="sidebarList">
           <Link to="/users" className="Link">
             <li className="sidebarListItem">
             <Person className="sidebarIcon"/> Users
             </li>
             </Link>
             <Link to="/products" className="Link">
             <li className="sidebarListItem">
                <Timeline className="sidebarIcon"/> Products
             </li>
             </Link>
             <li className="sidebarListItem">
                 <Receipt className="sidebarIcon"/>Transactions
             </li>
             <li className="sidebarListItem">
                 <Assessment className="sidebarIcon"/>Report
             </li>
        </ul> 
        </div>
        <div className="sidebarMenu">
         <h3 className="sidebarTitle">Notifications</h3> 
         <ul className="sidebarList">
             <li className="sidebarListItem">
             <Mail className="sidebarIcon"/> Mail
             </li>
             <li className="sidebarListItem">
                <Feedback className="sidebarIcon"/> Feedback
             </li>
             <li className="sidebarListItem">
                 <Message className="sidebarIcon"/>Messages
             </li>
        </ul> 
        </div>
        <div className="sidebarMenu">
         <h3 className="sidebarTitle">Staff</h3> 
         <ul className="sidebarList">
             <li className="sidebarListItem">
             <Person className="sidebarIcon"/> Manage
             </li>
             <li className="sidebarListItem">
                <Timeline className="sidebarIcon"/> Analytics
             </li>
             <li className="sidebarListItem">
                 <Assessment className="sidebarIcon"/>Reports
             </li>
        </ul> 
        </div>
      </div>
    </div>
  );
}
