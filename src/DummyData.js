export const UserData = [
    {
      name: 'Jan',
      Active_User: 1000,
    },
    {
        name: 'Feb',
        Active_User: 12000,
      },
      {
        name: 'Mar',
        Active_User: 1100,
      },
      {
        name: 'Apr',
        Active_User: 8000,
      },
      {
        name: 'May',
        Active_User: 7500,
      },
      {
        name: 'Jun',
        Active_User: 750,
      },
      {
        name: 'Jul',
        Active_User: 13500,
      },
      {
        name: 'Aug',
        Active_User: 1000,
      },
      {
        name: 'Sep',
        Active_User: 12300,
      },
      {
        name: 'Oct',
        Active_User: 1800,
      },
      {
        name: 'Nov',
        Active_User: 8800,
      },
      {
        name: 'Dec',
        Active_User: 9000,
      },
  ];

  export const ProductData = [
    {
      name: 'Jan',
      sales: 1000,
    },
    {
        name: 'Feb',
        sales: 12000,
      },
      {
        name: 'Mar',
        sales: 19000,
      }
     ];


  export const UserRows = [
    {
      id: 1,
      username: "Snow",
      avatar:
        "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8YmVhdXRpZnVsJTIwZ2lybCUyMHBob3RvfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80",
      email: "snow@gmail.com",
      status: "active",
      transaction: "$1120",
    },
    {
      id: 2,
      username: "Snow",
      avatar:
        "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8YmVhdXRpZnVsJTIwZ2lybCUyMHBob3RvfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80",
      email: "snow@gmail.com",
      status: "active",
      transaction: "$1120",
    },
    {
      id: 3,
      username: "Snow",
      avatar:
        "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8YmVhdXRpZnVsJTIwZ2lybCUyMHBob3RvfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80",
      email: "snow@gmail.com",
      status: "active",
      transaction: "$1120",
    },
    {
      id: 4,
      username: "Snow",
      avatar:
        "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8YmVhdXRpZnVsJTIwZ2lybCUyMHBob3RvfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80",
      email: "snow@gmail.com",
      status: "active",
      transaction: "$1120",
    },
    {
      id: 5,
      username: "Snow",
      avatar:
        "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8YmVhdXRpZnVsJTIwZ2lybCUyMHBob3RvfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80",
      email: "snow@gmail.com",
      status: "active",
      transaction: "$1120",
    },
    {
      id: 6,
      username: "Snow",
      avatar:
        "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8YmVhdXRpZnVsJTIwZ2lybCUyMHBob3RvfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80",
      email: "snow@gmail.com",
      status: "active",
      transaction: "$1120",
    },
    {
      id: 7,
      username: "Snow",
      avatar:
        "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8YmVhdXRpZnVsJTIwZ2lybCUyMHBob3RvfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80",
      email: "snow@gmail.com",
      status: "active",
      transaction: "$1120",
    },
    {
      id: 8,
      username: "Snow",
      avatar:
        "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8YmVhdXRpZnVsJTIwZ2lybCUyMHBob3RvfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80",
      email: "snow@gmail.com",
      status: "active",
      transaction: "$1120",
    },
    {
      id: 9,
      username: "Snow",
      avatar:
        "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8YmVhdXRpZnVsJTIwZ2lybCUyMHBob3RvfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80",
      email: "snow@gmail.com",
      status: "active",
      transaction: "$1120",
    },
    {
      id: 10,
      username: "Snow",
      avatar:
        "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8YmVhdXRpZnVsJTIwZ2lybCUyMHBob3RvfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80",
      email: "snow@gmail.com",
      status: "active",
      transaction: "$1120",
    }
  ];


  export const productRows = [
    {
      id: 1,
      product: "iPhone 13",
      img:
        "https://www.reliancedigital.in/medias/Apple-iPhone-13-Smartphone-491997700-i-1-1200Wx1200H?context=bWFzdGVyfGltYWdlc3wyNzUyNHxpbWFnZS9qcGVnfGltYWdlcy9oMDkvaDY3Lzk2MzQ2MDY1MTQyMDYuanBnfDMzNGQzMzU1Njc0Mjg0NTAyYWEyNmI5YzNjODgwYzljY2ViOWUyMDg3ZGUyMWZjMWNiNjVjNzljMmYwYzQwZGM",
      stock: 1267,
      status: "active",
      price: "$1000",
    },
    {
      id: 2,
      product: "iPhone 13",
      img:
        "https://www.reliancedigital.in/medias/Apple-iPhone-13-Smartphone-491997700-i-1-1200Wx1200H?context=bWFzdGVyfGltYWdlc3wyNzUyNHxpbWFnZS9qcGVnfGltYWdlcy9oMDkvaDY3Lzk2MzQ2MDY1MTQyMDYuanBnfDMzNGQzMzU1Njc0Mjg0NTAyYWEyNmI5YzNjODgwYzljY2ViOWUyMDg3ZGUyMWZjMWNiNjVjNzljMmYwYzQwZGM",
      stock: 1267,
      status: "active",
      price: "$1000",
    },
    {
      id: 3,
      product: "iPhone 13",
      img:
        "https://www.reliancedigital.in/medias/Apple-iPhone-13-Smartphone-491997700-i-1-1200Wx1200H?context=bWFzdGVyfGltYWdlc3wyNzUyNHxpbWFnZS9qcGVnfGltYWdlcy9oMDkvaDY3Lzk2MzQ2MDY1MTQyMDYuanBnfDMzNGQzMzU1Njc0Mjg0NTAyYWEyNmI5YzNjODgwYzljY2ViOWUyMDg3ZGUyMWZjMWNiNjVjNzljMmYwYzQwZGM",
      stock: 1267,
      status: "active",
      price: "$1000",
    },
    {
      id: 4,
      product: "iPhone 13",
      img:
        "https://www.reliancedigital.in/medias/Apple-iPhone-13-Smartphone-491997700-i-1-1200Wx1200H?context=bWFzdGVyfGltYWdlc3wyNzUyNHxpbWFnZS9qcGVnfGltYWdlcy9oMDkvaDY3Lzk2MzQ2MDY1MTQyMDYuanBnfDMzNGQzMzU1Njc0Mjg0NTAyYWEyNmI5YzNjODgwYzljY2ViOWUyMDg3ZGUyMWZjMWNiNjVjNzljMmYwYzQwZGM",
      stock: 1267,
      status: "active",
      price: "$1000",
    },
    {
      id: 5,
      product: "iPhone 13",
      img:
        "https://www.reliancedigital.in/medias/Apple-iPhone-13-Smartphone-491997700-i-1-1200Wx1200H?context=bWFzdGVyfGltYWdlc3wyNzUyNHxpbWFnZS9qcGVnfGltYWdlcy9oMDkvaDY3Lzk2MzQ2MDY1MTQyMDYuanBnfDMzNGQzMzU1Njc0Mjg0NTAyYWEyNmI5YzNjODgwYzljY2ViOWUyMDg3ZGUyMWZjMWNiNjVjNzljMmYwYzQwZGM",
      stock: 1267,
      status: "active",
      price: "$1000",
    },
    {
      id: 6,
      product: "iPhone 13",
      img:
        "https://www.reliancedigital.in/medias/Apple-iPhone-13-Smartphone-491997700-i-1-1200Wx1200H?context=bWFzdGVyfGltYWdlc3wyNzUyNHxpbWFnZS9qcGVnfGltYWdlcy9oMDkvaDY3Lzk2MzQ2MDY1MTQyMDYuanBnfDMzNGQzMzU1Njc0Mjg0NTAyYWEyNmI5YzNjODgwYzljY2ViOWUyMDg3ZGUyMWZjMWNiNjVjNzljMmYwYzQwZGM",
      stock: 1267,
      status: "active",
      price: "$1000",
    },
    {
      id: 7,
      product: "iPhone 13",
      img:
        "https://www.reliancedigital.in/medias/Apple-iPhone-13-Smartphone-491997700-i-1-1200Wx1200H?context=bWFzdGVyfGltYWdlc3wyNzUyNHxpbWFnZS9qcGVnfGltYWdlcy9oMDkvaDY3Lzk2MzQ2MDY1MTQyMDYuanBnfDMzNGQzMzU1Njc0Mjg0NTAyYWEyNmI5YzNjODgwYzljY2ViOWUyMDg3ZGUyMWZjMWNiNjVjNzljMmYwYzQwZGM",
      stock: 1267,
      status: "active",
      price: "$1000",
    },
    {
      id: 8,
      product: "iPhone 13",
      img:
        "https://www.reliancedigital.in/medias/Apple-iPhone-13-Smartphone-491997700-i-1-1200Wx1200H?context=bWFzdGVyfGltYWdlc3wyNzUyNHxpbWFnZS9qcGVnfGltYWdlcy9oMDkvaDY3Lzk2MzQ2MDY1MTQyMDYuanBnfDMzNGQzMzU1Njc0Mjg0NTAyYWEyNmI5YzNjODgwYzljY2ViOWUyMDg3ZGUyMWZjMWNiNjVjNzljMmYwYzQwZGM",
      stock: 1267,
      status: "active",
      price: "$1000",
    },
    {
      id: 9,
      product: "iPhone 13",
      img:
        "https://www.reliancedigital.in/medias/Apple-iPhone-13-Smartphone-491997700-i-1-1200Wx1200H?context=bWFzdGVyfGltYWdlc3wyNzUyNHxpbWFnZS9qcGVnfGltYWdlcy9oMDkvaDY3Lzk2MzQ2MDY1MTQyMDYuanBnfDMzNGQzMzU1Njc0Mjg0NTAyYWEyNmI5YzNjODgwYzljY2ViOWUyMDg3ZGUyMWZjMWNiNjVjNzljMmYwYzQwZGM",
      stock: 1267,
      status: "active",
      price: "$1000",
    },
    {
      id: 10,
      product: "iPhone 13",
      img:
        "https://www.reliancedigital.in/medias/Apple-iPhone-13-Smartphone-491997700-i-1-1200Wx1200H?context=bWFzdGVyfGltYWdlc3wyNzUyNHxpbWFnZS9qcGVnfGltYWdlcy9oMDkvaDY3Lzk2MzQ2MDY1MTQyMDYuanBnfDMzNGQzMzU1Njc0Mjg0NTAyYWEyNmI5YzNjODgwYzljY2ViOWUyMDg3ZGUyMWZjMWNiNjVjNzljMmYwYzQwZGM",
      stock: 1267,
      status: "active",
      price: "$1000",
    },
  ];

  