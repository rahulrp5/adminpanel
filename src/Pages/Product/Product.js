import React from 'react';
import {Link} from 'react-router-dom';
import Chart from '../../Components/Chart/Chart';
import {ProductData} from '../../DummyData';
import {Publish} from '@material-ui/icons';
import './Product.css';

export default function Product() {
    return (
        <div className="product">
            <div className="productTitleContainer">
                <h2 className="productTitle">Product</h2>
                <Link to='/newproduct'>
                <button className="productAddButton">Create</button>
                </Link>
            </div>
            <div className="productTop">
            <div className="productTopLeft">
                <Chart data={ProductData} dataKey="sales" title="Sales Performance"/>
            </div>
            <div className="productTopRight">
                <div className="productInfoTop">
            <img src="https://m.media-amazon.com/images/I/71vZLIfj5yS._SL1500_.jpg" className="productInfoImg" alt=""/>
                <span className="productName"> OnePlus LED TV</span>  
                </div>
                <div className="productInfoBottom">
                    <div className="productInfoItem">
                   <span className="productInfoKey">id:</span>
                   <span className="productInfoValue">123</span>
                    </div>
                    <div className="productInfoItem">
                   <span className="productInfoKey">sales</span>
                   <span className="productInfoValue">3453</span>
                    </div>
                    <div className="productInfoItem">
                   <span className="productInfoKey">active</span>
                   <span className="productInfoValue">yes</span>
                    </div>
                    <div className="productInfoItem">
                   <span className="productInfoKey">In stock:</span>
                   <span className="productInfoValue">no</span>
                    </div>
                </div>
            </div>
            </div>
            <div className="productBottom">
                <form className="productForm">
                 <div className="productFormLeft">
                     <label> Product Name  </label>
                     <input type = "text" placeholder="Product  Name"></input>
                     <label> In Stock</label>
                     <select name="inStock" id="inStock">
                         <option value="yes">Yes</option>
                         <option value="no">No</option>
                     </select>
                     <label> Active</label>
                     <select name="active" id="active">
                         <option value="yes">Yes</option>
                         <option value="no">No</option>
                     </select>
                 </div>
                 <div className="productFormRight">
                     <div className="productUpload">
                         <img src="https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-13-pro-family-hero?wid=940&hei=1112&fmt=png-alpha&.v=1631220221000" alt="" className="productUploadImg"/>
                     <label for="file"><Publish className="publish"/></label>
                     <input type="file" id="file"/>
                     </div>
                     <button className="productButton">Update</button>
                 </div>
                </form>
            </div>
        </div>
    )
}
