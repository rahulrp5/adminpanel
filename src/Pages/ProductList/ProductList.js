import React from 'react';
import './ProductList.css';
import { DataGrid } from "@material-ui/data-grid";
import { Delete } from "@material-ui/icons";
import {productRows} from '../../DummyData';
import { Link } from "react-router-dom";
import { useState } from "react";

export default function ProductList() {
    const [data, setData] = useState (productRows);

    const Handledelete = (id)=>{
        setData(data.filter((item) => item.id !== id));
      }
      const columns = [
        { field: "id", headerName: "ID", width: 90 },
        { field: "product", headerName: "Product", width: 140, 
        renderCell: (param)=>{
            return(
                <div className="productListItem">
                    <img src={param.row.img} alt="" className="productListImg"/>
                    {param.row.product}
                </div>
            )
        }
     },
        { field: "stock", headerName: "Stock", width: 230 },
        {
          field: "status",
          headerName: "Status",
          width: 130,
        },
        {
            field: "price",
            headerName: "Price",
            width: 190,
          },
          {
          field: "action",
            headerName: "Action",
            width: 160,
            renderCell:(params) =>{
                return(
                    <div>
                     <Link to={"/product/"+params.row.id}>
                    <button className="productListEdit">Edit</button>
                    </Link>
                    <Delete className="productListDelete" onClick={()=>Handledelete(params.row.id)}/>
                    </div>
                )
            }
          },
      ];
    return (
        <div className="ProductList">
            <DataGrid 
        rows={data} disableSelectionOnClick
        columns={columns}
        pageSize={8}
        rowsPerPageOptions={[5]}
        checkboxSelection
      />
        </div>
    )
}
