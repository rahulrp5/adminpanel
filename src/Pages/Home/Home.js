import React from 'react';
import FeaturedInfo from '../../Components/FeaturedInfo/FeaturedInfo';
import Chart from '../../Components/Chart/Chart';
import {UserData} from '../../DummyData';
import './Home.css';
import WidgetSm from '../../Components/WidgetSm/WidgetSm';
import WidgetLg from '../../Components/WidgetSm/WidgetLg';

export default function Home() {
    return (
        <div className="Home">
           <FeaturedInfo/>
           <Chart  data={UserData} title="User Analytics" grid dataKey="Active_User" />
           <div className="homeWidgets">
               <WidgetSm/>
               <WidgetLg/>
           </div>
        </div>
    )
}
