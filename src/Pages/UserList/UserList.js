import React from "react";
import "./UserList.css";
import { DataGrid } from "@material-ui/data-grid";
import { Delete } from "@material-ui/icons";
import {UserRows} from '../../DummyData';
import { Link } from "react-router-dom";
import { useState } from "react";

export default function UserList() {
  const [data, setData] = useState(UserRows);

  const Handledelete = (id)=>{
    setData(data.filter((item) => item.id !== id));
  }
  const columns = [
    { field: "id", headerName: "ID", width: 90 },
    { field: "username", headerName: "Username", width: 140, 
    renderCell: (param)=>{
        return(
            <div className="userListUser">
                <img src={param.row.avatar} alt="" className="userListImg"/>
                {param.row.username}
            </div>
        )
    }
 },
    { field: "email", headerName: "Email", width: 230 },
    {
      field: "status",
      headerName: "Status",
      width: 130,
    },
    {
        field: "transaction",
        headerName: "Transaction Volume",
        width: 190,
      },
      {
      field: "action",
        headerName: "Action",
        width: 160,
        renderCell:(params) =>{
            return(
                <div>
                 <Link to={"/user/"+params.row.id}>
                <button className="userListEdit">Edit</button>
                </Link>
                <Delete className="userListDelete" onClick={()=>Handledelete(params.row.id)}/>
                </div>
            )
        }
      },
  ];

  return (
    <div className="userList" >
      <DataGrid 
        rows={data} disableSelectionOnClick
        columns={columns}
        pageSize={8}
        rowsPerPageOptions={[5]}
        checkboxSelection
      />
    </div>
  );
}
