import React from 'react';
import {Link} from 'react-router-dom';
import './Users.css';
import {PermIdentity, CalendarToday, Email, Phone, LocationSearching, Publish} from '@material-ui/icons';

export default function Users() {
    return (
        <div className="Users">
           <div className="userTitleContainer">
               <h1 className="userTitle">Edit User</h1>
               <Link to='/newUser'>
               <button className="userAddButton">Create</button>
               </Link>
           </div>
           <div className="userContainer">
               <div className="userShow">
               <div className="userShowTop">
                     <div className="userShowTopTitle">
                       <img src="https://image.shutterstock.com/image-photo/portrait-smiling-handsome-man-grey-260nw-1288638850.jpg" alt="" className="userShowImg"/>
                           <div className="userShowyyg">
                           <span className="userShowUsername">Rahul</span>
                           <span className="userShowUserTitle">Software Engineer</span>
                           </div>
                       </div>
                   <div className="userShowBottom">
                       <span className="userShowBottomTitle">Account Details</span>
                       <div className="userShowInfo">
                       <PermIdentity className="userShowIcon"/>
                       <span className="userShowBottomInfoTitle">Rahul</span>
                       </div>
                       <div className="userShowInfo">
                       <CalendarToday className="userShowIcon"/>
                       <span className="userShowBottomInfoDob">01/07/2007</span>
                       </div>
                       <span className="userShowBottomTitle">Contact Details</span>
                       <div className="userShowInfo">
                       <Phone className="userShowIcon"/>
                       <span className="userShowBottomInfoPhoneNumber">+91-9875643210</span>
                       </div>
                       <div className="userShowInfo">
                       <Email className="userShowIcon"/>
                       <span className="userShowBottomInfoEmail">Rahul@gmail.com</span>
                       </div>
                       <div className="userShowInfo">
                       <LocationSearching className="userShowIcon"/>
                       <span className="userShowBottomInfoAddress">Indore | India</span>
                       </div>
                   </div>
               </div>
               </div>
               <div className="userShowUpdate">
                   <span className="userUpdateTitle">Edit</span>
                   <form className="userUpdateForm">
                       <div className="userUpdateLeft">
                           <div className="userUpdateItem">
                               <label>Username</label>
                               <input type="text" placeholder="Rahul" className="userUpdateInput"/>
                           </div>
                           <div className="userUpdateItem">
                               <label>Full Name</label>
                               <input type="text" placeholder="Rahul P" className="userUpdateInput"/>
                           </div>
                           <div className="userUpdateItem">
                               <label>Date of Birth</label>
                               <input type="text" placeholder="01/07/2007" className="userUpdateInput"/>
                           </div>
                           <div className="userUpdateItem">
                               <label>Phone Number</label>
                               <input type="text" placeholder="+91-9875643210" className="userUpdateInput"/>
                           </div>
                           <div className="userUpdateItem">
                               <label>Email</label>
                               <input type="text" placeholder="Rahul@gmail.com" className="userUpdateInput"/>
                           </div>
                           <div className="userUpdateItem">
                               <label>Address</label>
                               <input type="text" placeholder="Indore | India" className="userUpdateInput"/>
                           </div>
                       </div>
                       <div className="userUpdateRight">
                           <div className="userUpdateUpload">
                               <img src="https://image.shutterstock.com/image-photo/portrait-smiling-handsome-man-grey-260nw-1288638850.jpg" alt="" className="userUpdateImg"/>
                              <label for="file"><Publish className="userUpdateIcon"/></label>
                               <input type="file" id="file"/>
                           </div>
                           <button className="userUpdateButton">Update</button>
                       </div>
                   </form>
               </div>
           </div>
        </div>
    )
}
